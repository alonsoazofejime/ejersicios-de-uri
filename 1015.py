#Ejersicio #15
#Entrada: Contiene dos l�neas de datos, la primera contiene dos valores double: x1 y1, la segunda tambi�n contiene dos valores double con un d�gito despu�s del punto: x2 y2.
#Salida: Calcular y mostrar el valor de la distancia usando la f�rmula provista, con 4 d�gitos despu�s del punto.
#Que hace?: Ejecuta la formula que calcula entre las 2 cordenadas de 2 puntos.
#________________________________________________________________#
import math

X1,Y1=input().split()
X2,Y2=input().split()
#Definimos por medio de 2 renglones y en cada uno separado po un espacio
x1=float(X1)
y1=float(Y1)
x2=float(X2)
y2=float(Y2)
#Todas las entradas tienen 2 deimales

resultado = math.sqrt((x2-x1)**2+(y2-y1)**2)
#Relizzamos la operacion, com el math.sqrt es para realizar la raiz cuadrada, porque lo habia untentado realizar con  **2 y no funcionaba

print('{0:.4f}'.format(resultado))



