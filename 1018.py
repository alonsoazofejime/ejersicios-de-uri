#Ejercicio 1018
#Entrada: La entrada contiene un valor entero N (0 < N < 1000000).
#Salida: Imprimir el n�mero le�do y la cantidad m�nima necesaria de billetes en lenguaje portugu�s, como muestra el ejemplo. No olvides imprimir el final de l�nea luego de cada l�nea, de otra forma recibir�s �Presentation Error�.
#Que hace?:En este problema tienes que leer un valor entero y calcular el menor n�mero posible de billetes en que puede ser descompuesto. Los billetes posibles son 100, 50, 20, 10, 5, 2 y 1. Imprimir el valor le�do y la lista de billetes.

##########################################################

def billetes100(polo):
#obtener el cociente
	return polo//100
#obtener el cociente
def billetes50(polo):
#obtener el cociente
	return polo//50
def billetes20(polo):
#obtener el cociente
	return polo//20
#obtener el cociente
def billetes10(polo):
#obtener el cociente
	return polo//10
def billetes5(polo):
#obtener el cociente
	return polo//5
def billetes2(polo):
#obtener el cociente
	return polo//2
def billetes1(polo):
#obtener el cociente
	return polo//1
#definimos que la entrada es un numero entero
entrada = int(input()) 
print(entrada)
print(str(billetes100(entrada))+" nota(s) de R$ 100,00")
A = entrada - billetes100(entrada)*100
print(str(billetes50(A))+" nota(s) de R$ 50,00")
B = A - billetes50(A)*50
print(str(billetes20(B))+" nota(s) de R$ 20,00")
C = B - billetes20(B)*20
print(str(billetes10(C))+" nota(s) de R$ 10,00")
D = C - billetes10(C)*10
print(str(billetes5(D))+" nota(s) de R$ 5,00")
E = D - billetes5(D)*5
print(str(billetes2(E))+" nota(s) de R$ 2,00")
F = E - billetes2(E)*2
print(str(billetes1(F))+" nota(s) de R$ 1,00")
