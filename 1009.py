#Ejersicio #1009
#Entrada: La entrada contiene un texto (primer nombre del empleado), y dos valores de doble precisión, los cuales representan el salario del vendedor y el valor total vendido por él.
#Salida: Imprimir el salario total del vendedor, de acuerdo a los ejemplos.
#Que hace?: Escriba un programa que lea el nombre de un vendedor, su salario fijo y el total de las ventas realizadas por él en el mes (en dinero). Considerando que el vendedor recibe un 15% de los productos vendidos, escribir el salario final (total) de cada vendedor a fin de mes, con dos cifras decimales.

#____________________________________________________________________#

A=input()
B=float(input())
C=float(input())
#definimos las entradas que ceben de contener decimales
D=C*15/100+B #Realizamos la operacion
print("TOTAL = R$ "+"{0:.2f}".format(D))
#{0:.1f} se usa para definir cuantos decimales queremos en el resultado que G que para poder llamar lo usamos el .format