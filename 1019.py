#Ejersicio 1019
#Entrada: Un valor entero que representa los segundos
#Salida:Imprimir el tiempo leído del archivo de entrada (segundos) convertido en horas:minutos:segundos como el ejemplo siguiente
#Que hace?: Leer un valor entero, que es la duración en segundos de un evento realizado en una fábrica, e informarlo expresado en horas:minutos:segundos.

#_______________________________________________________________________________#
variable = int(input())
horas = int(variable/60/60)#primero se divide entre 60 para pasar de segundos a minutos, lugo se repite el proceso para pasar a horas, todo esto tiene que dar numeros enteros
minutos = int((variable / 60) - (horas * 60)) #Transformamos la variable dividiendo por 60 para transformar a minutos, luego de esto le quitamos los minutos que representan horas todo esto debe de dar resultado numeros enteros
segundos = int(variable - ((horas * 60 * 60) + (minutos * 60))) # Ahora a los segundos totales le quitamos los minutos que representan horas y minutos
print(str(horas)+':'+str(minutos)+':'+str(segundos))
