#1011
# Entrada: un valor real con el radio de una esfera
# Salida: el volumen de la esfera
# Que hace?:Haga un programa que calcule y muestre el volumen de una esfera mediante un radio (R) dado. La f�rmula para calcular dicho volumen es: (4/3) * pi * R3. Considere asignar a Pi el valor: 3.14159.

#__________________________________________________________________#
def volumen(radio):
	pi = 3.14159
	return (4/3)*pi*radio**3
 
entrada = int(input())
print("VOLUME = "+'{0:.3f}'.format(volumen(entrada)))
