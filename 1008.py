#Ejersicio #1008
#Entrada: El archivo de entrada contiene 2 n�meros enteros y 1 valor de punto flotante, representando el n�mero, cantidad de horas trabajadas y el monto recibido del empleado por hora trabajada.
#Salida: Imprimir el n�mero y salario del empleado, acorde al siguiente ejemplo, con los espacios en blanco antes y despu�s del signo igual.
#Que hace? lee un n�mero de empleado, su n�mero de horas trabajadas en el mes y el monto recibido por hora. Imprimir el n�mero de empleado y el salario que �l/ella recibir� a fin de mes, con dos lugares decimales.

#_________________________________________________________________________#

A = input()
B = int(input())
C = float(input())
resultado = B*C
print("NUMBER = "+(A)) #imprime el numero de empleado
print("SALARY = U$ "+"{0:.2f}".format(resultado)) # imprimer el salario de ese empleado con base al calculo ya realizado
