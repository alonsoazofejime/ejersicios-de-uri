#1016
#Entrada: valor entero representando los kilometros recorridos
#Salida: los minutos que tardo durante el trayecto
#Restriccion: numeros con decimales o numeros no reales
#Que hace?: Dos autos (X e Y) parten en la misma direcci�n. El auto X parte con una velocidad constante de 60 km/h y el auto Y parte con velocidad constante de 90 km/h.

#En una hora (60 minutos), el auto Y se separa una distancia de 30 kil�metros del auto X, en otras palabras, se aleja un kil�metro cada 2 minutos.

#Leer la distancia (en kil�metros) y calcular que tiempo le lleva (en minutos) al auto Y tomar esa distancia en relaci�n con el otro auto.

km = int(input())

def VOLKSBUS(km):
	return km*2
	
print(str(VOLKSBUS(km))+" minutos")
