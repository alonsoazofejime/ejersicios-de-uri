#Ejersicio #1013
#Entrada:El archivo de entrada contiene 3 valores enteros.
#Salida: Imprimir el mas grande de los 3 valores seguido por un espacio y el mensaje "eh o maior".
#Que hace?: Realiza una ecuacion.

#_____________________________________________________________________________________#

def mayor(x,y,z):
	if x>y and x>z:
		MAIOR1 = x
		return (MAIOR1)
#colocamos la segunda función
	if y>x and y>z:
		MAIOR2 = y
		return (MAIOR2)
	if z>x and z>y:
		MAIOR3 = z
		return (MAIOR3)
		#Como entrada son tres valores enteros
a,b,c = input().split(" ")
x = int(a)
y = int(b)
z = int(c)
#He probado que cuando se trabaja con entradas, note que no importa si ponemos las entradas al inicio o al final
#imprimimos la función
print(str(mayor(x,y,z))+" eh o maior")
