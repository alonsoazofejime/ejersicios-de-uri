#1017
#Entrada: dos numeros enteros que representan el tiempo y la velocidad media
#Salida: el valor que representa el combustible necesario para el viaje, con 3 decimales
#Restriccion: los numeros deben ser enteros reales
#Que hace?:Juancito quiere calcular y mostrar la cantidad de litros de combustible gastado en un viaje, con un auto que hace 12 Km/L. Para eso, le gustaría que lo ayudes a través de un programa sencillo. Para realizar el cálculo, tienes que leer el tiempo (en horas) y la velocidad media (en Km/h) del viaje. De esta forma se puede obtener la distancia, y luego, calcular la cantidad de litros necesarios. Mostrar el valor con tres dígitos luego del punto decimal.

##################################################_______________________________########################################

t = int(input())
vm = int(input())

def poyopawa(t,vm):
	km = 12
	return (vm/km)*t

print ('{0:.3f}'.format(poyopawa(t,vm)))
