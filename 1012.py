#1012
#Entrada:La entrada contiene tres valores de doble precisión con un dígito luego del punto decimal.
#Salida:La salida contiene 5 renglones. Cada uno de los renglones corresponde a las áreas descriptas anteriormente, siempre con el mensaje correspondiente (en portugués) y un espacio entre los dos puntos y el valor. El valor calculado debe ser presentado con 3 dígitos luego del punto decimal.
#Que hace?: un programa que lea tres valores de punto flotante: A, B y C. Luego, calcular y mostrar:
#               a) El área del triángulo rectángulo de base A y altura C.
#               b) El área del círculo de radio C (Pi = 3.14159).
#               c) El área del trapecio el cual tiene A y B como bases, y C como altura.
#               d) El área del cuadrado de lado B.
#               e) El área del rectángulo de lados A y B.

#________________________________________________________________#
valores = input()
a,b,c = valores.split()
#Convertir los valores a que se reciban con decimales
a = float(a)
b = float(b)
c = float(c)

#definimos los funciones de areas con las figuras correspondientes
def areaTriangulo(baseA, alturaC):
	return baseA*alturaC/2
# se area correspondinte

def areaCirculo(radioC):
	pi = 3.14159
	return pi*radioC*radioC

def areaTrapezio(baseA, baseB, alturaC):
	return ((baseA+baseB)*alturaC)/2

def areaCuadrado(ladoB):
	return ladoB*ladoB

def areaRectrangulo(baseA, alturaB):
	return baseA*alturaB
#Se definieron varias funciones que realizaran una operacion determinada
	


#se convoca la funcion llamando a los valores a, b y c correspondientes a cada funcion
print("TRIANGULO: "+'{0:.3f}'.format(areaTriangulo(a,c)))
print("CIRCULO: "+'{0:.3f}'.format(areaCirculo(c)))
print("TRAPEZIO: "+'{0:.3f}'.format(areaTrapezio(a,b,c)))
print("QUADRADO: "+'{0:.3f}'.format(areaCuadrado(b)))
print("RETANGULO: "+'{0:.3f}'.format(areaRectrangulo(a,b)))
#queremos que los resultados se impriman con 3 decimales

