#Ejersicio 1005
#Entrada: 2 numeros con con puntos flotantes, estos se digitan separados por un renglon
#Salida: Un numero con punto flotante resultado de una operacion realizada con las entradas
#¿Que hace?: El primer numero los muntiplica por 3.5 y luego lo suma con la multiplicacion del segundo numero por 7.5

#_________________________________________________________________________________________________________________________#

A=float(input())
B=float(input())
#definimos la sentrada que tengan decimales
C=A*3.5+B*7.5 #Realizamos la operacion
D=C/11 #Realizamos la otra operacion

print("MEDIA = "+'{0:.5f}'.format(D)) #{0:.1f} se usa para definir cuantos decimales queremos en el resultado que G que para poder llamar lo usamos el .format


