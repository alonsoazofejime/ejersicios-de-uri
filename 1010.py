#Ejersicio 1010
#Entrada: El archivo de entrada contiene dos l�neas de datos. En cada l�nea habr� 3 valores: Dos enteros y un valor flotante con 2 d�gitos despu�s del punto decimal.
#Salida: El archivo de entrada contiene dos l�neas de datos. En cada l�nea habr� 3 valores: Dos enteros y un valor flotante con 2 d�gitos despu�s del punto decimal.
#�Que hace?: En este problema, la tarea consiste en leer un c�digo de un producto 1, el n�mero de unidades del producto 1, el precio de una unidad de producto 1, el c�digo de un producto 2, el n�mero de unidades del producto 2 y el precio de una unidad de producto 2. Despu�s de esto, calcular y mostrar la cantidad a pagar.

#_________________________________________________________________________________________________________#
A,B,C=input().split()
D,E,F=input().split()
#definimos las entradas usalndo el .plit para poder poner multiples entradas en un solo renglon
A=int(A)
B=int(B)
C=float(C)
D=int(D)
E=int(E)
F=float(F)
#otorgamos a las entradas valores distintos porque algunos de ellos deben de ser numeor sneteros y otros con decimales
resultado=B*C+E*F # realizamos la operacion
print("VALOR A PAGAR: R$ "+'{0:.2f}'.format(resultado))
#{0:.1f} se usa para definir cuantos decimales queremos en el resultado que G que para poder llamar lo usamos el .format

