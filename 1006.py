#Ejercisio 1006
#Entrada: 3 valores de punto flotante con un dígito decimal después de la coma.
#Salida: Imprime MEDIA(Promedio en Portugués) de acuerdo con el siguiente ejemplo, con un espacio en blanco antes y después del signo igual.
#Que hace?: Leer tres valores (variables A, B y C), que son las tres notas de estudiantes. Entonces, calcule el promedio, considerando que la nota A tiene peso 2, la nota B tiene peso 3 y la nota C tiene peso 5. 

#__________________________________________________________________________________#

A=float(input())
B=float(input())
C=float(input())
#definimos que las entradas deben de ser contener decimales
D=A*2/10# D sera el resultado de la multiplicacion de A por 2 dividido por 10
E=B*3/10# E es el resultdo de la multiplicacion de B por tres cuartos
F=C*5/10# F es el resultado de la multiplicacion de C por cinco decimos
G=D+E+F# G es el reultado de la suma de las operaciones anteriores
print("MEDIA = "+"{0:.1f}".format(G))

#{0:.1f} se usa para definir cuantos decimales queremos en el resultado que G que para poder llamar lo usamos el .format
